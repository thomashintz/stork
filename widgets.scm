(define-syntax widget
  (syntax-rules ()
    ((_ name markup_ attributes_)
     (add-widget name `((markup . `(*TOP* markup_)) (attributes . attributes_))))))

(widget
 'stylesheet
 (link (@ (href ,(string-append "/res/css/" path)) (type "text/css")
          (rel "stylesheet")))
 ((path "")))

(widget
 'row
 (div (@ (class "row")) ,@contents)
 ())

(widget
 'column-layout
 ,(let ((class (++ "column small-" (number->string
                                    (inexact->exact (round (/ 12 (length contents))))))))
    `(div (@ (class "row"))
          ,@(map (lambda (col)
                   `(div (@ (class ,class)) ,col))
                 contents)
          ,(if fill-row `(div (@ (class "column end"))) "")))
 ((fill-row #t)))

(widget
 'panel
 (div (@ (class "panel")) ,@contents)
 ())

(widget
 'panel-small
 (div (@ (class "panel panel-small")) ,@contents)
 ())

(widget
 'text-center
 (div (@ (class "text-center")) ,@contents)
 ())

(widget
 'group-box
 (div (@ (class "panel group-box"))
      (h4 (@ (class "group-box-title")) ,title)
      ,@contents)
 ((title "")))

(widget
 'radio-button
 (label (input (@ (type "radio") (name ,name))) ,label)
 ((name "radio1") (label "radio1")))

(widget
 'spacer
 (div (@ (class "spacer")) ,@contents)
 ())

(widget
 'modal
 (div (@ (class "modal-overlay"))
      (div (@ (class "modal-content"))
           ,@contents))
 ())
