function handleLambda(target, k, sid, args, success) {
    return function() {
        // merge AJAX data args together
        var allArgs = { 'k': k, 'sid': sid };
        if (args) { for (var attr in args) { allArgs[attr] = args[attr](); }}
        $.ajax({ type: 'POST',
                 url: '/ajax/k',
                 data: allArgs,
                 success: function(resp) {
                     if (target) {
                         $(target).html(resp);
                     }
                     if (success) { success(); }
                 }
               });
    };
}

/*{ 'selector': selector,
    'event': event,
    'k': k,
    'sid': sid,
    'target': target,
    'success': success, // proc to run after AJAX request completes
    'args': args } */
function installHandler(_) {
    $(_.selector)
        .on(_.event,
            handleLambda(_.target, _.k, _.sid, _.args, _.success));
}
