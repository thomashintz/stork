(import chicken scheme posix)
(use chicken-syntax)
(use spiffy srfi-18 intarweb uri-common tcp)

(include "websockets")
(import websockets)

(websocket-connection-timeout 0)
(websocket-ping-interval 0)
(websocket-drop-incoming-pings #f)

(define (make-websocket-handler)
  (lambda (spiffy-continue)
    (if (equal? (cadr (uri-path (request-uri (current-request)))) "web-socket")
        (with-websocket
         (lambda ()
           (let loop ()
             (receive (data type) (websocket-receive)
                      (unless (eq? type 'connection-close)
                              (if (eq? type 'ping)
                                  (websocket-send 'pong data)
                                  (websocket-send type data))
                              (loop))))))
        (spiffy-continue))))

(vhost-map `(("localhost" . ,(make-websocket-handler))))

(debug-log (current-output-port))
(root-path ".")
(server-port 8080)
;(tcp-buffer-size 4096)
(start-server)


