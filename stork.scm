;(declare (uses stateful-web))

(import chicken scheme srfi-1)
(use awful waffle http-session srfi-69 coops miscmacros continuations format
     spock synch srfi-18 awful-sse mailbox json numbers spiffy spiffy-cookies json)
(load "stateful-web.scm")
(import stateful-web)
(load "websockets.scm")
(import websockets)

(enable-sxml #f)
(enable-session #t)
(enable-ajax #t)

(ajax-library "/res/js/vendor/jquery.js")

(define *actions* '())
(define *games* '())
(define *games-mutex* (make-mutex "games mutex"))
(define *players* '())
(define *players-mutex* (make-mutex "games mutex"))

(define ++ string-append)
(set! format:complex-numbers #t)

(define round
  (let ((original-round round))
    (lambda (n #!optional (places 0))
      (if (or (= places 0) (= n 0))
          (original-round n)
          (let ((factor (expt 10 places)))
            ; use format to get trailing zeros
            (string->number
             (format #f "~v$" places
                     (/ (original-round (* n factor)) factor))))))))

; Find the length up to n.
; More efficient form of (> (length l) x)
(define (length-up-to l n)
  (define (walk-list l i)
    (if (or (= i n) (null? l))
        i
        (walk-list (cdr l) (+ i 1))))
  (walk-list l 0))

(define (length-at-least? l n)
  (= (length-up-to l n) n))

; Same as srfi-1 take but returns early instead of
; erroring when it runs out of elements.
(define (take-up-to l n)
  (take l (length-up-to l n)))

; Average change in percent between elements of l (list)
(define (find-avg-change l)
  (define (_ l-remaining avg)
    (if (<= (length l-remaining) 1)
        avg
        (_ (cdr l-remaining)
           (+ avg
              (/ (cadr l-remaining) (car l-remaining))))))
  (if (length-at-least? l 2)
      (exact->inexact (- (/ (_ l 0) (- (length l) 1)) 1))
      0))

(define-class <player> ()
  ((name initform: (symbol->string (gensym)) accessor: player-name)
   (mailbox initform: (make-mailbox (gensym)) accessor: player-mailbox)
   (turn-finished initform: #f accessor: player-turn-finished)))

(define-class <game> ()
  ((stocks initform: '() accessor: game-stocks)
   (players initform: '() accessor: game-players)
   (name initform: (string-append "game" (number->string (random 1000000)))
         accessor: game-name)
   (mutex initform: (make-mutex "a game mutex") accessor: game-mutex)))


(define-constant max-stock-stability 100)

(define-class <stock> ()
  ((name initform: "unnamed" accessor: stock-name)
   (price initform: 0 accessor: stock-price)
   (avg-change initform: 0 accessor: stock-avg-change)
   (price-changes initform: 0 accessor: stock-price-changes)
   (price-history initform: '() accessor: stock-price-history)
   (last-price initform: 0 accessor: stock-last-price)
   (stability initform: (random max-stock-stability) accessor: stock-stability)
   (turns-at-current-stability initform: 1 accessor: stock-turns-at-current-stability)))

(define (make-stock name price)
  (make <stock> 'name name 'price price 'price-history `(,price)))

(define-method ((setter stock-price) before: (stock <stock>) new-price)
  (set! (stock-last-price stock) (stock-price stock)))

(define-method ((setter stock-price) after: (stock <stock>) new-price)
  (set! (stock-price-history stock) (cons new-price (stock-price-history stock)))
  (set! (stock-price-changes stock) (+ (stock-price-changes stock) 1))
  (set! (stock-avg-change stock) (/ (+ (* (stock-avg-change stock)
                                         (- (stock-price-changes stock) 1))
                                       (stock-percent-change stock))
                                   (stock-price-changes stock)))
  (set! (stock-turns-at-current-stability stock)
        (+ (stock-turns-at-current-stability stock) 1))
  (if (= 1 (random (stock-stability stock))) (set! (stock-stability stock)
                                                   (random max-stock-stability))))

(define-method ((setter stock-stability) after: (stock <stock>) new-stability)
  (set! (stock-turns-at-current-stability stock) 1))

(define (decimal->pct n #!optional (places 2))
 (format #f "~:$%" (round (* n 100) places)))

(define (number->money n) (format #f "$~:$" (round n 2)))

(define-method (stock-percent-change (stock <stock>))
  (if (or (= (stock-price stock) 0) (= (stock-last-price stock) 0))
      0
      (- (/ (stock-price stock)
            (stock-last-price stock))
         1)))

(define-method (stock-average-change (stock <stock>) n)
  (let ((last-n-prices (reverse (take-up-to (stock-price-history stock) n))))
    (find-avg-change last-n-prices)))

(define-method (generate-new-stock-price (stock <stock>))
  (define (random-sign) (if (= (random 2) 0) -1 1))
  (define (random-pct-change max)
    (* (* (random (inexact->exact (round (* max 100000000)))) 0.00000001) (random-sign)))

  (define initial-pct-variance 0.3) ; how much the stock can flucuate
  (define mean-lifetime 100)        ; how many turns until 1/e pct remains (~0.367)
  (define (exponential-decay initial time mean-lifetime)
    (* initial (exp (/ (- time) mean-lifetime))))

  (define (correct-for-low-price? price)
    (define threshold 10)         ; price must be below $10
    (define correction-chance 20) ; 1/20 chance of kicking in
    (if (< price threshold)
        (= 1 (random correction-chance))
        #f))
  (define (generate-low-price-correction)
    (define max-random-price 20)
    (define min-change 10)
    (+ (random max-random-price) min-change))

  (define (skew-pct stock)
    (define cap 0.3)
    (define over-cap-stability-adjustment 5)
    (define impact-of-skew 0.3) ; skew changes result by 30%
    (let ((avg (stock-average-change stock 5)))
      (* impact-of-skew
         (if (> (abs avg) cap)
             (begin (set! (stock-stability stock) over-cap-stability-adjustment)
                    (* cap (if (< avg 0) (- 1) 1))) ; negative if needed
             avg))))

  (let ((price (stock-price stock)))
    (if (correct-for-low-price? price)
        (generate-low-price-correction)
        (+ (* price
              (+ (random-pct-change
                  (exponential-decay initial-pct-variance
                                     (+ (stock-turns-at-current-stability stock) 1)
                                     mean-lifetime))
                 (skew-pct stock)))
           price))))

(define-syntax widget
  (syntax-rules ()
    ((_ name markup_ attributes_)
     (add-widget name `((markup . `(*TOP* markup_)) (attributes . attributes_))))))

(load "widgets")

(widget
 'buy-or-sell-center
 (group-box
  (@ (title " Buy or Sell"))
  (div
   ,(map (lambda (stock)
           `(label (@ (class "inline-label"))
                   (input (@ (type "radio") (value ,(stock-name stock))
                             (name "buy-stock-name-input")
                             ,(if (string=? (stock-name stock) selected-stock) 'checked '()))
                          " " ,(stock-name stock))
                   (br))) stocks))
  (div (column-layout
        (label "shares: "
               (input (@ (id "number-of-shares") (type "text") (name "shares"))))
        (label "money: "
               (input (@ (type "text") (name "money") (id "buy-money-input")))))
       (column-layout (button (@ (class "button tiny expand") (name "buy-sell")
                                 (id "buy-shares") (type "submit")) "buy")
                      (button (@ (class "button tiny expand") (name "buy-sell")
                                 (id "sell-shares") (type "submit")) "sell"))))
 ((stocks '()) (selected-stock '())))

(widget
 'assets-center
  (group-box
   (@ (title "Assets"))
   (p "cash: " ,(number->money cash))
   (p " net: " ,(number->money
                 (+ (fold (lambda (stock net)
                            (+ net
                               (* (cdr
                                   (assoc (string->symbol
                                           (stock-name stock))
                                          (shares)))
                                  (stock-price stock))))
                          0
                          stocks)
                    cash))))
  ((shares '()) (stocks '()) (cash 0)))


(widget
 'info-message
 (group-box (@ (title ,title))
            (p ,contents)
            (form (input (@ (type "hidden") (name "k") (value ,url)))
                  (button (@ (type "submit")) "OK")))
 ((url "") (title "") (message "")))

(widget
 'error-message
 (info-message (@ (title "Error") (url ,url)) ,@contents)
 ((url "")))

(widget
 'enter-single-info
 (group-box (@ (title ,title))
            (form (input (@ (type "hidden") (name "k") (value ,url)))
                  (input (@ (type "hidden") (name "arg") (value "empty")))
                  ,text ": " (input (@ (name ,val-name) (autofocus)))))
 ((url "") (title "") (text "") (val-name "val")))

(widget
 'game-info
 (column-layout
  (panel-small
   (text-center
    (column-layout
     (strong ,player-name)
     (strong ,game-name)
     (strong (a (@ (href "javascript:void(0)") (id "save-game")) "save")
             ,(register-ajax-function "#save-game" "click" save-game-proc))
     (strong (a (@ (href "/")) "exit"))))))
 ((player-name "unknown player") (game-name "unknown game") (save-game-proc (lambda () 'nil))))

(widget
 'turn-center
 (group-box (@ (title "Turn"))
            (ul ,(map (lambda (player) `(li (,(player-name player) ": "
                                        ,(if (player-turn-finished player)
                                             "turn finished"
                                             "playing"))))
                      (filter (lambda (p) (not (string=? (player-name p)
                                                    (player-name current-player))))
                              players)))
            (button (@ (class "tiny") (id "finish-turn-button"))
                    "finish turn"))
 ((players '()) (current-player 'uninitialized)))

(widget
 'buy-sell-shares
 (row
  (div (@ (class "column small-6"))
       (buy-or-sell-center (@ (stocks ,stocks) (selected-stock ,(stock-name (car stocks))))))
  (div (@ (class "column small-3"))
       (assets-center (@ (stocks ,stocks) (shares ,shares) (cash ,cash))))
  (div (@ (class "column small-3"))
       (div (@ (id "turn-center"))
            (turn-center (@ (players ,players) (current-player ,current-player))))))
 ((stocks '()) (cash 0) (shares (lambda () '())) (players '()) (current-player 'uninitialized)))


(widget
 'stock-info
 (div (@ (class ,(++ "stock-info-container " (if (>= change 0) "stock-up" "stock-down"))))
      (group-box (@ (title ,name))
                 (p "price: " ,(number->money price) (br)
                    "stability: " ,stability " for " ,turns " turns")
                 (p "change: " ,(decimal->pct change) (br)
                    "average: " ,(decimal->pct avg))
                 (p "shares: " ,shares (br)
                    "value: " ,(number->money (* price shares)))
                 (a (@ (href "javascript:void(0)") (class "show-chart") (id ,name))
                    "show chart")))
 ((name "stock name") (price -1) (change -1) (shares -1) (avg -1) (stability 100) (turns 0)))

(widget
 'stock-chart
 (group-box (@ (title "Stock history"))
            (div (@ (id ,id) (class "stock-chart")))
            (a (@ (href "javascript:void(0)") (id "close-stock-chart")) "close"))
 ((id "no-id")))

(widget
 'games
 (column-layout (group-box (@ (title "Games available"))
                           ,(if (null? games)
                                '(p "No games available. Try starting a new one.")
                                `(ul
                                  ,@(map (lambda (game)
                                           `(li
                                             ,(let ((id (symbol->string (gensym))))
                                                `((a (@ (href "javascript:void(0)")
                                                        (id ,id))
                                                     ,(game-name game))
                                                  ,(register-ajax-function
                                                    (++ "#" id) "click"
                                                    (lambda () (join-game-proc game))
                                                    target: "#app-container")))))
                                         games)))
                           (br)
                           (button (@ (id "start-game")) "start new game")))
 ((games '()) (join-game-proc (lambda (game) (void)))))

(widget
 'app
 (div
  (game-info (@ (player-name ,(if (not (eq? current-player 'uninitialized))
                                  (player-name current-player) ""))
                (game-name ,game-name)))
  ; shares is in a thunk otherwise it gets interpreted as SXML
  (div (@ (id "buy-sell-shares-container"))
       (column-layout
        (buy-sell-shares (@ (stocks ,stocks) (cash ,cash) (shares ,(lambda () shares))
                            (players ,players) (current-player ,current-player)))))
  (div (@ (id "stock-chart-container"))
       (column-layout (stock-chart (@ (id "stock-chart")))))
  (column-layout
   ,@(map (lambda (stock)
            `(stock-info (@ (name ,(stock-name stock))
                            (price ,(stock-price stock))
                            (stability ,(stock-stability stock))
                            (turns ,(stock-turns-at-current-stability stock))
                            (change ,(stock-percent-change stock))
                            (avg ,(stock-average-change stock 5))
                            (shares ,(cdr (assoc (string->symbol (stock-name stock))
                                                 shares))))))
          stocks))
  ,@contents)
 ((cash 0) (stocks '()) (run-sim-url "") (shares '()) (players '())
  (current-player 'uninitialized) (game-name "")))

(widget
 'app-container
 (div (@ (id "app-container")) ,@contents)
 ())

(define-syntax show-message
  (syntax-rules ()
    ((_ exit-to title message ...)
     (exit/url/once exit-to url
     `(info-message (@ (title ,title) (url ,url)) message ...)))))

(define-syntax show-error
  (syntax-rules ()
    ((_ exit-to message ...)
     (exit/url/once exit-to url
     `(error-message (@ (url ,url)) message ...)))))

(define-syntax get-value
  (syntax-rules ()
    ((_ exit-to the-title the-text)
     (begin (exit/url/once
             exit-to url
             `(app-container
               (enter-single-info (@ (url ,url)
                                     (title the-title)
                                     (text the-text)
                                     (val-name "val")))))
            ($ 'val)))))

(define (defpage path thunk)
  (define-page path
    (lambda ()
      (thunk))
    headers: (with-output-to-string
               (lambda () (waffle-sxml->html
                      `((stylesheet (@ (path "normalize.css")))
                        (stylesheet (@ (path "foundation.min.css")))
                        (stylesheet (@ (path "stork.css")))
                        (script (@ (language "javascript") (type "text/javascript")
                                   (src "/res/js/spock-runtime-debug.js")))
                        (script (@ (language "javascript") (type "text/javascript")
                                   (src "/res/js/vendor/jquery.flot.js")))
                        (script (@ (language "javascript") (type "text/javascript")
                                   (src "/res/js/app.js")))
                        (script (@ (language "javascript") (type "text/javascript")
                                   (src "/res/js/stork.js")))
                        (script (@ (language "javascript") (type "text/javascript")
                                   (src "/lambda.js")))))))
    no-session: #t))

(define (app player)
  (define (begin-game #!key
                      (cash 10000)
                      (stocks (map (lambda (n) (make-stock n (+ (random 995) 5)))
                                   '("google" "netflix" "amazon" "twitter")))
                      shares game)
    (with-exit begin-game
      (let* ((new-game? (equal? game #f))
             (shares
              (if shares shares
                  (map (lambda (stock) `(,(string->symbol (stock-name stock)) . 0)) stocks)))
             (game (if game
                       (begin (synch (game-mutex game)
                                     (set! (game-players game)
                                           (cons player (game-players game))))
                              game)
                       (make <game> 'players `(,player) 'stocks stocks
                             'name (get-value begin-game "Game name" "name")))))
        (when new-game? (set! *games* (cons game *games*)))

        ($session-set! 'game-mutex (game-mutex game))

        (define (stock-chart-javascript)
          (define (get-points stock)
            (let ((i 0))
              (list->vector
               (map (lambda (price) (set! i (+ i 1)) `#(,i ,price))
                    (reverse (stock-price-history stock))))))
          `(literal
            ,(<spock>
              (begin
                (on-page-loaded
                 (lambda ()
                   (begin
                     (on ($ "#close-stock-chart") "click"
                         (lambda ()
                           (%inline .show ($ "#buy-sell-shares-container"))
                           (%inline .hide ($ "#stock-chart-container"))))
                     (map
                      (lambda (stock)
                        (on ($ (jstring (string-append "#" (car stock)))) "click"
                            (lambda ()
                              (begin
                                (%inline .hide ($ "#buy-sell-shares-container"))
                                (%inline .show ($ "#stock-chart-container"))
                                (plot-chart (list->vector (cdr stock)))))))
                      '#^(map (lambda (s) `(,(stock-name s) ,(get-points s))) stocks)))))))))

        (define (buy-sell-shares-javascript)
          `(literal
            ,(<spock>
              (begin
                (define stocks '#^(map (lambda (s) `(,(stock-name s) . ,(stock-price s))) stocks))
                (define (number->money n) (* (round (* n 100)) 0.01))
                (define (selected-stock)
                  (%inline .val ($ "input[name=buy-stock-name-input]:checked")))
                (define (shares) (string->number (%inline .val ($ "#number-of-shares"))))
                (define (set-shares n) (%inline .val ($ "#number-of-shares") n))
                (define (money) (string->number (%inline .val ($ "#buy-money-input"))))
                (define (set-money n) (%inline .val ($ "#buy-money-input") n))
                (define (stock-price stock-name) (cdr (assoc stock-name stocks string=?)))
                (define (shares->money)
                  (number->money (* (shares) (stock-price (selected-stock)))))
                (define (money->shares)
                  (floor (/ (money) (stock-price (selected-stock)))))
                (let ((last-edited 'shares))
                  (on-page-loaded
                   (lambda ()
                     (on ($ "input[name=buy-stock-name-input]") "click"
                         (lambda ()
                           (if (eq? last-edited 'shares)
                               (set-money (shares->money))
                               (set-shares (money->shares)))))
                     (on ($ "#number-of-shares") "keyup"
                         (lambda ()
                           (set! last-edited 'shares)
                           (set-money (shares->money))))
                     (on ($ "#buy-money-input") "keyup"
                         (lambda ()
                           (set! last-edited 'money)
                           (set-shares (money->shares)))))))))))

        (define (all-players-finished? game)
          (not (any (cut eq? <> #f) (map (cut player-turn-finished <>) (game-players game)))))

        (define (reset-players-finished-status)
          (for-each (lambda (p) (set! (player-turn-finished p) #f)) (game-players game)))

        (define (buy-sell-shares)
          (with-exit
           buy-sell
           (let ((name (string->symbol ($ 'name)))
                 (n (string->number ($ 'number)))
                 (type (string->symbol ($ 'type))))
             (set! n (if (eq? type 'buy) n (- n)))
             (let ((new-cash (+ cash (* (- n)
                                        (stock-price
                                         (find (lambda (s) (string=? (stock-name s) ($ 'name)))
                                               stocks))))))
               (cond ((< new-cash 0)
                      (show-error buy-sell "Purchase price greater than remaining "
                                  "cash. Please retry with a lower value."))
                     ((and (eq? type 'sell) (> (abs n) (cdr (assoc name shares))))
                      (show-error buy-sell "Not enough shares."))
                     (else
                      (begin (set! cash new-cash)
                             (set! shares (delete-duplicates
                                           (cons `(,name . ,(+ (cdr (assoc name shares)) n))
                                                 shares)
                                           (lambda (x y) (eq? (car x) (car y))))))))))
           (main-page)))
        (define (generate-new-prices)
          (repeat 1
                  (for-each (lambda (s) (set! (stock-price s) (generate-new-stock-price s))) stocks)))

        (define (setup-global-ajax-callbacks)
          `(,(register-ajax-function
              "#buy-shares" "click" buy-sell-shares target: "#app-container"
              args: '((name . "$('input[name=buy-stock-name-input]:checked').val()")
                      (number . "$('#number-of-shares').val()")
                      (type . "'buy'")))
            ,(register-ajax-function
              "#sell-shares" "click" buy-sell-shares target: "#app-container"
              args: '((name . "$('input[name=buy-stock-name-input]:checked').val()")
                      (number . "$('#number-of-shares').val()")
                      (type . "'sell'")))))
        (define (finish-turn)
          (set! (player-turn-finished player) #t)
          (if (all-players-finished? game)
              (begin (reset-players-finished-status)
                     (generate-new-prices)
                     (main-page))
              (begin (for-each
                      (lambda (p)
                        (unless (equal? player p)
                                (mailbox-send!
                                 (player-mailbox p)
                                 `((action . "replace")
                                   (target . "turn-center")
                                   (content .
                                         ,(with-output-to-string
                                            (lambda ()
                                              (waffle-sxml->html
                                               `(turn-center (@ (players ,(game-players game))
                                                                (current-player ,player)))))))))))
                      (game-players game))
                     (main-page))))
        (define (main-page)
          `(app-container
            ((app (@ (cash ,cash) (stocks ,stocks) (shares ,shares)
                     (players ,(game-players game)) (current-player ,player)
                     (game-name ,(game-name game)))
                  ,(buy-sell-shares-javascript)
                  ,(stock-chart-javascript))
             ,(setup-global-ajax-callbacks)
             ,(register-ajax-function "#finish-turn-button" "click"
                                      finish-turn target: "#app-container"))))

        (begin `((script (@ (type "text/javascript"))
                         ,(let ((k (k->url main-page)))
                            (++ "history.pushState({ k: " k  " }, '', '/?k=" k "');")))
                 ,(main-page))))))
  (define (join-game the-game)
    (begin-game game: the-game stocks: (game-stocks the-game)))
  (define (show-available-games)
    `((games (@ (games ,*games*) (join-game-proc ,join-game)))
      ,(register-ajax-function "#start-game" "click" begin-game target: "#app-container")))
  (show-available-games))

; just to set up the end point. The names and event are meaningless
(ajax "k" 'xxyyzz 'fubar
      (lambda ()
        (with-output-to-string
          (lambda ()
            (waffle-sxml->html (process-continuations))))))



(defpage "/"
  (lambda ()
    (when (not (session-valid? (sid)))
          (sid (session-create))
          ((session-cookie-setter) (sid)))
    (with-output-to-string
      (lambda ()
        (waffle-sxml->html
         (or (process-continuations (lambda () (redirect-to "/")))
             (with-exit start
                        `(app-container
                          ,(app
                            (if (session-ref (sid) 'player #f)
                                (session-ref (sid) 'player)
                                (let ((player
                                       (make <player>
                                         'name (get-value start "Your name" "name"))))
                                  (session-set! (sid) 'player player)
                                  player)))))))))))

(define (get-sid)
  (read-cookie "awful-cookie"))

(define-page "/web-socket"
  (lambda ()
    (with-websocket
     (lambda ()
       (websocket-send-message "SYN")
       (log-to (error-log) (websocket-receive-message))))))

; each player gets their own mailbox
; when a player clicks finish turn
; send a message to all the other players
; need to somehow pass the player object
; into here or move this inside the main
; app function
;; (define (application-code ws)
;;   (and-let* ((player (session-ref (get-sid) 'player #f))
;;              (mb (player-mailbox player))
;;              (ws-name (gensym)))
;;             (log-to (error-log) "~A: read by ~A" (websocket-read ws) ws-name)
;;             (websocket-send ws "ACK")
;;             (log-to (error-log) "going.")
;;             (let loop ()
;;               (let ((r (mailbox-receive! mb)))
;;                 (log-to (error-log) "received by websocket ~A: ~A"
;;                         ws-name (->string r))
;;                 (websocket-send ws
;;                                 (with-output-to-string
;;                                   (lambda ()
;;                                     (json-write
;;                                      (list->vector r))))))
;;               (loop))
;;             (websocket-close ws)))

;; (vhost-map `(("localhost" . ,(make-websocket-handler application-code '(/ "web-socket")))))


(define (make-sse-proc mailbox)
  (lambda ()
    (let loop ()
      (let ((msg (mailbox-receive! mailbox 10 #f)))
        (when msg
              (send-sse-data (json-write (list->vector msg))))))))

;; (handle-exception
;;   (lambda (exn chain)
;;     (log-to (error-log) "type: ~A~%~%[~A] \"~A ~A HTTP/~A.~A\" ~A"
;;             (condition->list exn)
;;             (seconds->string (current-seconds))
;;             (request-method (current-request))
;;             (uri->string (request-uri (current-request)))
;;             (request-major (current-request))
;;             (request-minor (current-request))
;;             (build-error-message exn chain #t))
;;      (send-status 'internal-server-error)))
