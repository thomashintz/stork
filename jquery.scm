(define (http-get url handler)
  ((native (%host-ref 'jQuery.get)) (jstring url) (callback handler)))

(define ($ selector)
  (%inline jQuery selector))

(define (jval jobj)
  (%inline .val jobj))

(define (jtext jobj)
  (%inline .text jobj))

(define (jtext! jobj text)
  (%inline .text jobj (jstring text)))

(define (jhtml jobj)
  (%inline .html jobj))

(define (jhtml! jobj html)
  (%inline .html jobj (jstring html)))

(define (on jobj event handler)
  (%inline .on jobj (jstring event) (callback handler)))
