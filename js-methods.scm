(define-native document)

(define (console-log val)
  (%inline "console.log" val))

(define (on-page-loaded proc)
  (%inline "$(document).ready" (callback proc)))

(define (plot-chart points)
  (%inline "$.plot" "#stock-chart" points))

(define-syntax-rule
  (literal-object prop val)
  (let ((obj (%inline "new Object")))
    (%property-set! prop obj val)
    obj))
