(module stateful-web
  (k->url k-table-clear! k-table-del!
   process-continuations register-function register-ajax-function
   with-exit exit/url exit/url/once exit/url/only exit/url/only/once)

(import chicken scheme data-structures extras srfi-1 srfi-13)
(use awful http-session srfi-69 bshift-breset spiffy)

(define *global-vars* (make-hash-table))
(define *k-table* (gensym)) ; prevent possible conflict in session table

(define (generate-guid)
  (fold (lambda (e out) (string-append (number->string (random 99999999)) out))
        ""
        '(1 2 3 4 5)))

(define (k-table)
  (let ((k-table (session-ref (sid) *k-table* #f)))
    (unless k-table
            (set! k-table (make-hash-table string=)))
    (session-set! (sid) *k-table* k-table)
    k-table))

(define (k-table-set! k-url k args)
  (hash-table-set! (k-table) k-url `((k . ,k) (args . ,args))))

(define (k-table-del! k-url)
  (hash-table-delete! (k-table) k-url))

(define (k-table-clear!)
  (session-set! (sid) *k-table* (make-hash-table string=)))

(define (k-table-ref k-url #!optional default)
  (hash-table-ref/default (k-table) k-url default))

(define (k->url k . args)
  (let ((k-url (generate-guid)))
    (k-table-set! k-url k args)
    k-url))

(define-syntax with-exit
  (syntax-rules ()
    ((_ exit-name body ...)
     (breset exit-name body ...))))

(define-syntax exit/url
  (syntax-rules ()
    ((_ exit-name k-url-var body ...)
     (bshift exit-name k
             (let ((k-url-var (k->url k 'nil)))
               body ...)))))

;;; THIS ACTUALLY DELETES THE NEXT CONTINUATION?
(define-syntax exit/url/once
  (syntax-rules ()
    ((_ exit-name k-url-var body ...)
     (let ((_k-url 'nil))
       (exit/url exit-name k-url-var (set! _k-url k-url-var) body ...)
       (k-table-del! _k-url)))))

(define-syntax exit/url/only
  (syntax-rules ()
    ((_ exit-name k-url-var body ...)
     (begin (k-table-clear!)
            (exit/url exit-name k-url-var body ...)))))

(define-syntax exit/url/only/once
  (syntax-rules ()
    ((_ exit-name k-url-var body ...)
     (begin (k-table-clear!)
            (exit/url exit-name k-url-var body ...
                      (k-table-del! k-url-var))))))

(define (register-function proc)
  (k->url proc))

; Checks for a continuation or function that matches the 'k' request
; parameter. If found invokes it passing in args.
; Returns three values.
; 1: result of continuation/function (if found, else #f)
; 2: whether or not the 'k' parameter exists.
; 3: whether or not the continuation or function was found in the k table.
(define (process-continuations #!optional expired-proc filter-proc)
  (let ((k ($ 'k #f))
        (arg ($ 'arg #f)))
    (if k
        (let* ((proc/args (k-table-ref k #f)))
          (if proc/args
              (values (apply (cdr (assoc 'k proc/args)) (cdr (assoc 'args proc/args))) #t #t)
              (values (or (and expired-proc (expired-proc))
                          (lambda () "unknown or expired page"))
                      #t #f)))
        (values #f #f #f))))

(define (register-ajax-function selector event proc #!key args success target)
  `(script (@ (language "javascript") (type "text/javascript"))
           (literal
            ,(++ "installHandler({'selector':'" selector "','"
                                  "event':'" event "','"
                                  "k':'" (register-function proc) "','"
                                  "sid':'" (sid) "'"
                                  (if target
                                      (++ ",'target':'" target "'")
                                      "")
                                  (if args
                                      (++ ",'args':"
                                          "{"
                                          (string-intersperse
                                           (map (lambda (var/val)
                                                  (conc "'" (car var/val)
                                                        "':function() { return "
                                                        (cdr var/val)
                                                        ";}"))
                                                args)
                                           ",")
                                          "}")
                                      "")
                                  (if success
                                      (++ ",'success':" success)
                                      "")
                                  "});"))))

(define (global-set! var val)
  (hash-table-set! *global-vars* var val))

(define (global-get var)
  (hash-table-ref *global-vars* var))

(define (global-ref var)
  (hash-table-ref/default *global-vars* var #f))

)
