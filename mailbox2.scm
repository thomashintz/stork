(module mailbox2
 (make-mailbox mailbox-send mailbox-receive)

(import chicken scheme)
(use srfi-18 data-structures)

(define (make-mailbox)
  `((mutex . ,(make-mutex)) (queue . ,(make-queue)) (condvar . ,(make-condition-variable))))

(define (mailbox-send mailbox obj #!optional timeout default)
  (let ((mutex (cdr (assoc 'mutex mailbox))))
    (dynamic-wind
        (lambda () (mutex-lock! mutex))
        (lambda ()
          (queue-add! (cdr (assoc 'queue mailbox)) obj))
        (lambda () (mutex-unlock! mutex))))
  (condition-variable-signal! (cdr (assoc 'condvar mailbox))))

(define (mailbox-receive mailbox #!optional timeout default)
  (let ((mutex (cdr (assoc 'mutex mailbox))))
    (mutex-lock! mutex)
    (if (queue-empty? (cdr (assoc 'queue mailbox)))
        (mutex-unlock! mutex (cdr (assoc 'convar mailbox)))
        (let ((obj (queue-remove! (cdr (assoc 'queue mailbox)))))
          (mutex-unlock! mutex)
          obj))))

)

;; (import chicken scheme)
;; (use srfi-18)
;; (import mailbox2)

;; (define mb (make-mailbox))
;; (define t1 (make-thread
;;             (lambda ()
;;               (mailbox-send mb "hi")
;;               (mailbox-send mb "que pasa?")
;;               (print (mailbox-receive mb) (thread-name (current-thread)))
;;               (mailbox-send mb "hi2")
;;               (thread-sleep! 1)
;;               (mailbox-send mb "que pasa?2")
;;               (print (mailbox-receive mb) (thread-name (current-thread)))) "t1"))
;; (define t2 (make-thread
;;             (lambda ()
;;               (mailbox-send mb "hello")
;;               (print (mailbox-receive mb) (thread-name (current-thread)))
;;               (mailbox-send mb "wow")
;;               (print (mailbox-receive mb) (thread-name (current-thread))))
;;             "t2"))
;; (define t3 (make-thread
;;             (lambda ()
;;               (print (mailbox-receive mb) (thread-name (current-thread)))
;;               (print (mailbox-receive mb) (thread-name (current-thread))))
;;             "t3"))

;; (thread-start! t1)
;; (thread-start! t2)
;; (thread-start! t3)
;; (thread-join! t1)
;; (thread-join! t2)
;; (thread-join! t3)
;; (print "done")
