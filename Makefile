SPOCK = chicken-spock

res/js/app.js: sxml-html.scm jquery.scm js-methods.scm
	$(SPOCK) sxml-html.scm jquery.scm js-methods.scm > res/js/app.js

run: res/js/app.js
	awful --development-mode stork.scm

clean:
	rm -f *~ res/js/app.js

.PHONE: run clean
