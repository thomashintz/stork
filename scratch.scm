(display
(<spock>
(begin
;; (display
;; (<spock>
;;  (begin
;;    (define (square x) (* x x))
;;    (print (square #^(read)))))
;; )

;;  (define (http-get url handler)
;;    ((native (%host-ref 'jQuery.get)) (jstring url) (callback handler)))

;;  (define ($ selector)
;;    (%inline jQuery selector))

;;  (define (jval jobj)
;;    (%inline .val jobj))

;;  (define (jtext jobj)
;;    (%inline .text jobj))

;;  (define (jtext! jobj text)
;;    (%inline .text jobj (jstring text)))

;;  (define (jhtml jobj)
;;    (%inline .html jobj))

;;  (define (jhtml! jobj html)
;;    (%inline .html jobj (jstring html)))

;;  (define (on jobj event handler)
;;    (%inline .on jobj (jstring event) (callback handler)))

  ;; (begin (define x #^cash)
  ;;        (define-entry-point draw
  ;;          (%inline "$.plot" "#stock-chart" '#(#(1 3) #(3 6) #(6 2))))
  ;;        (console-log x)))))

(define (console-log val)
  (%inline "console.log" val))

(define (on-page-loaded proc)
  (%inline .ready (%inline "$" document) (callback proc)))

;(console-log (.foo bar))
;(%inline "history.pushState" (.foo bar)) ;(stateObj, "page 2", "bar.html");

(let ((obj2 (new Number)))
  (%property-set! "foo" obj2 "bar")
  (console-log obj2))

)))
